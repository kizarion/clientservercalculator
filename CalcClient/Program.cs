﻿using System;
using CalcClient.CalcServiceRef;

namespace CalcClient
{
    internal static class Program
    {
        private static void Main()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("1. AddFormula");
                Console.WriteLine("2. GetResult");
                Console.WriteLine("0. Exit");
                Console.WriteLine("---------------");
                Console.Write("Selection: ");
                var menuSelect = Console.ReadLine();
                switch (menuSelect)
                {
                    case "1":
                    {
                        CaseAddFormula();
                    }
                        break;
                    case "2":
                    {
                        CaseGetResult();
                    }
                        break;
                    case "0": return;
                }
            }
        }

        private static void CaseAddFormula()
        {
            Console.Clear();
            Console.WriteLine("--AddFormula--");
            Console.Write("Variable name: ");
            var variable = Console.ReadLine();
            Console.Write("Formula: ");
            var formula = Console.ReadLine();
            Console.WriteLine("---------------");
            Console.Write("Service response: ");
            using (var client = new CalcServiceClient())
            {
                var response = client.AddFormula(variable, formula);
                Console.WriteLine(response);
                Console.ReadLine();
            }
        }

        private static void CaseGetResult()
        {
            Console.Clear();
            Console.WriteLine("--GetResult--");
            Console.Write("Variable name: ");
            var variable = Console.ReadLine();
            Console.WriteLine("---------------");
            Console.Write("Service response: ");
            using (var client = new CalcServiceClient())
            {
                var response = client.GetResult(variable);
                Console.WriteLine(response);
                Console.ReadLine();
            }
        }
    }
}