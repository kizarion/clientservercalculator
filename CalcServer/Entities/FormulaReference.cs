﻿namespace CalcServer.Entities
{
    public class FormulaReference
    {
        public int Id { get; set; }
        public FormulaRecord Formula { get; set; }
        public string ReferencedVariable { get; set; }
    }
}