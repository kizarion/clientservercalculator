﻿using System.ComponentModel.DataAnnotations.Schema;
using CalcServer.Enums;

namespace CalcServer.Entities
{
    public class FormulaRecord
    {
        public int Id { get; set; }

        //Variables should be unique to avoid ambiguity
        [Index(IsUnique = true)] public string Variable { get; set; }

        public string Formula { get; set; }
        public double Result { get; set; }
        public Status Status { get; set; }
    }
}