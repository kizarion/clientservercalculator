﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using CalcServer.Data;
using CalcServer.Entities;
using CalcServer.Enums;
using org.mariuszgromada.math.mxparser;

namespace CalcServer.Helpers
{
    public static class ProcessingHelper
    {
        public static bool IsFormulaValid(string formula)
        {
            // Removing whitespaces for further checks
            var cleanFormula = formula.Replace(" ", "");

            // Validate that string only contains accepted symbols
            var symbolCheck = Regex.Match(cleanFormula, @"^([a-zA-Z+\-\/0-9()*])$").Success;

            // Validate that no empty parentheses exist
            var emptyCheck = !Regex.Match(cleanFormula, @"\(\)").Success;

            // Validate that operators dont follow each other
            var operatorCheck = !Regex.Match(cleanFormula, @"[\-\+\/\*]{2}").Success;

            // Validate that variables dont have numbers mixed in
            var variableCheck = !Regex.Match(cleanFormula, @"[a-zA-Z][0-9]|[0-9][a-zA-Z]").Success;

            // Validate parentheses balance
            var balanceCheck = IsParenthesesBalanced(cleanFormula);

            return symbolCheck
                   && emptyCheck
                   && operatorCheck
                   && balanceCheck
                   && variableCheck;
        }

        private static bool IsParenthesesBalanced(string formula)
        {
            var allowedChars = new HashSet<char>(new[] {'(', ')'});
            // Push only parentheses to stack, Reverse to keep pop sequence left to right
            var stack = new Stack<char>(formula.Reverse().Where(c => allowedChars.Contains(c)));
            var openParentheses = 0;
            while (stack.Any())
            {
                var c = stack.Pop();

                if (c == '(') openParentheses++;
                if (c == ')') openParentheses--;

                // Closing parentheses appeared before opening one
                if (openParentheses < 0) return false;
            }

            return openParentheses == 0;
        }

        public static string[] ExtractReferencedVariablesFromValidFormula(string formula)
        {
            // Get all variables, since formula expected on input is validated prior, checks are omitted
            var matchCollection = Regex.Matches(formula, @"[a-zA-Z]+").Cast<Match>().ToArray();
            // Cast result to string[]
            var result = matchCollection.Select(x => x.Value).Distinct().ToArray();
            return result;
        }

        public static void ProcessFormula(int formulaId, ProcessingQueue queue)
        {
            using (var taskContext = new DataContext())
            {
                var taskFormula = taskContext.FormulaRecords.SingleOrDefault(x => x.Id == formulaId);
                if (taskFormula == null) return;

                var validation = IsFormulaValid(taskFormula.Formula);

                // Load imitation
                Thread.Sleep(10000);

                if (validation == false)
                {
                    taskFormula.Status = Status.MalformedFormula;
                }
                else
                {
                    var referencedVariables = ExtractReferencedVariablesFromValidFormula(taskFormula.Formula);

                    // Saving relation to other variables in database
                    foreach (var referencedVariable in referencedVariables)
                        taskContext.FormulaReferences.Add(new FormulaReference
                            {Formula = taskFormula, ReferencedVariable = referencedVariable});
                    taskContext.SaveChanges();
                    var referencedFormulas = taskContext.FormulaRecords
                        .Where(x => referencedVariables.Contains(x.Variable)).ToList();

                    // If formula references more variables than records found in database then some variables are missing
                    if (referencedFormulas.Count < referencedVariables.Length)
                    {
                        taskFormula.Status = Status.MissingVariable;
                    }
                    else
                    {
                        var arguments = new List<Argument>();
                        var worstStatus = Status.Ready;

                        // Check for referenced formula statuses
                        foreach (var formula in referencedFormulas)
                        {
                            if (formula.Status == Status.Ready)
                                arguments.Add(new Argument($"{formula.Variable}={formula.Result}"));
                            if (formula.Status > worstStatus) worstStatus = formula.Status;
                        }

                        if (worstStatus == Status.Ready)
                        {
                            var e = new Expression(taskFormula.Formula, arguments.ToArray());
                            var result = e.calculate();
                            // NaN returned in case of division by zero
                            if (double.IsNaN(result))
                            {
                                taskFormula.Status = Status.MathException;
                            }
                            else
                            {
                                taskFormula.Result = result;
                                taskFormula.Status = Status.Ready;
                            }

                            taskContext.SaveChanges();

                            var dependantFormulas = taskContext.FormulaReferences
                                .Where(x => x.ReferencedVariable == taskFormula.Variable).Select(x => x.Formula)
                                .ToList();
                            foreach (var formula in dependantFormulas)
                            {
                                formula.Status = Status.Processing;
                                taskContext.SaveChanges();
                                queue.Enqueue(formula.Id);
                            }
                        }
                    }
                }

                taskContext.SaveChanges();
            }
        }
    }
}