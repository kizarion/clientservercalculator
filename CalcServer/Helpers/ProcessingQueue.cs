﻿using System.Collections.Generic;
using System.Threading;

namespace CalcServer.Helpers
{
    public class ProcessingQueue
    {
        private bool _delegateQueuedOrRunning;
        private readonly Queue<int> _jobs = new Queue<int>();

        public void Enqueue(int formulaId)
        {
            lock (_jobs)
            {
                _jobs.Enqueue(formulaId);
                if (_delegateQueuedOrRunning) return;
                _delegateQueuedOrRunning = true;
                ThreadPool.QueueUserWorkItem(ProcessQueuedItems, null);
            }
        }

        private void ProcessQueuedItems(object ignored)
        {
            while (true)
            {
                int formulaId;
                lock (_jobs)
                {
                    if (_jobs.Count == 0)
                    {
                        _delegateQueuedOrRunning = false;
                        break;
                    }

                    formulaId = _jobs.Dequeue();
                }

                try
                {
                    ProcessingHelper.ProcessFormula(formulaId, this);
                }
                catch
                {
                    ThreadPool.QueueUserWorkItem(ProcessQueuedItems, null);
                    throw;
                }
            }
        }
    }
}