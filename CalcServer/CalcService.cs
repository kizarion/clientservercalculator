﻿using System.ServiceModel;

namespace CalcServer
{
    [ServiceContract]
    public interface ICalcService
    {
        [OperationContract]
        string AddFormula(string variable, string formula);

        [OperationContract]
        string GetResult(string variable);
    }
}