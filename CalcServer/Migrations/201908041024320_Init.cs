using System.Data.Entity.Migrations;

namespace CalcServer.Migrations
{
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                    "dbo.FormulaRecords",
                    c => new
                    {
                        Id = c.Int(false, true),
                        Variable = c.String(maxLength: 100),
                        Formula = c.String(),
                        Result = c.Double(false),
                        Status = c.Int(false)
                    })
                .PrimaryKey(t => t.Id);

            CreateTable(
                    "dbo.FormulaReferences",
                    c => new
                    {
                        Id = c.Int(false, true),
                        ReferencedVariable = c.String(),
                        Formula_Id = c.Int()
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.FormulaRecords", t => t.Formula_Id)
                .Index(t => t.Formula_Id);
        }

        public override void Down()
        {
            DropForeignKey("dbo.FormulaReferences", "Formula_Id", "dbo.FormulaRecords");
            DropIndex("dbo.FormulaReferences", new[] {"Formula_Id"});
            DropTable("dbo.FormulaReferences");
            DropTable("dbo.FormulaRecords");
        }
    }
}