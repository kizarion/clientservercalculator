using System.Data.Entity.Migrations;

namespace CalcServer.Migrations
{
    public partial class UniqueVariableConstraint : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.FormulaRecords", "Variable", true);
        }

        public override void Down()
        {
            DropIndex("dbo.FormulaRecords", new[] {"Variable"});
        }
    }
}