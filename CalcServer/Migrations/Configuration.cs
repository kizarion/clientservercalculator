using System.Data.Entity.Migrations;
using CalcServer.Data;

namespace CalcServer.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataContext context)
        {

        }
    }
}