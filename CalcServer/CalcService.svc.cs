﻿using System;
using System.Linq;
using System.Threading;
using CalcServer.Data;
using CalcServer.Entities;
using CalcServer.Enums;
using CalcServer.Helpers;

namespace CalcServer
{
    public class CalcService : ICalcService
    {
        private readonly ProcessingQueue _processingQueue = new ProcessingQueue();

        public string AddFormula(string variable, string formula)
        {
            if (string.IsNullOrWhiteSpace(variable))
                return "Variable can't be empty";
            if (string.IsNullOrWhiteSpace(formula))
                return "Formula can't be empty";
            if (!variable.All(char.IsLetter))
                return "Unaccepted variable name";
            using (var context = new DataContext())
            {
                var count = context.FormulaRecords.Count(x => x.Variable == variable.ToLower());
                if (count != 0) return "Variable with this name already exists";
                var newFormula = new FormulaRecord
                {
                    Variable = variable.ToLower(),
                    Formula = formula,
                    Status = Status.Processing
                };

                context.FormulaRecords.Add(newFormula);
                context.SaveChanges();

                _processingQueue.Enqueue(newFormula.Id);

                return $"Added: {variable}={formula}";
            }
        }

        public string GetResult(string variable)
        {
            if (variable == null)
                return "Variable can't be empty";
            using (var context = new DataContext())
            {
                if (!context.FormulaRecords.Select(x => x.Variable == variable.ToLower()).Any())
                    return "Variable not found";
                {
                    var result = context.FormulaRecords.FirstOrDefault(x => x.Variable == variable.ToLower());


                    if (result.Status == Status.Processing)
                        while (result.Status == Status.Processing)
                            using (var dataContext = new DataContext())
                            {
                                try
                                {
                                    result = dataContext.FormulaRecords.FirstOrDefault(x =>
                                        x.Variable == variable.ToLower());
                                    Thread.Sleep(2000);
                                }
                                catch (TimeoutException)
                                {
                                    return "Request took more than a minute, aborting";
                                }
                            }

                    if (result.Status == Status.Ready)
                        return $"{variable.ToLower()}={result.Result}";
                    if (result.Status == Status.MissingVariable)
                        return $"Formula {result.Variable}={result.Formula} has missing arguments";
                    if (result.Status == Status.MathException)
                        return $"Formula {result.Variable}={result.Formula} raised an exception during processing";
                    if (result.Status == Status.MalformedFormula)
                        return $"Formula {result.Variable}={result.Formula} is malformed.";
                }

                return "Variable not found";
            }
        }
    }
}