﻿using System.Data.Entity;
using CalcServer.Entities;

namespace CalcServer.Data
{
    public class DataContext : DbContext
    {
        public DataContext()
        {
            Database.SetInitializer<DataContext>(null);
        }

        public virtual DbSet<FormulaRecord> FormulaRecords { get; set; }
        public virtual DbSet<FormulaReference> FormulaReferences { get; set; }
    }
}