﻿namespace CalcServer.Enums
{
    public enum Status
    {
        Ready = 0,
        Processing = 1,
        MissingVariable = 2,
        MathException = 3,
        MalformedFormula = 4
    }
}