﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CalcServer.Helpers.Tests
{
    [TestClass]
    public class ProcessingHelperTests
    {
        [TestMethod]
        public void ExtractReferencedVariablesFromValidFormulaTest()
        {
            // Arrange
            var formula = "x+915/y-zz*sk*ef-911";
            string[] expected = {"x", "y", "zz", "sk", "ef"};
            // Act
            var actual = ProcessingHelper.ExtractReferencedVariablesFromValidFormula(formula);
            // Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ProcessFormulaTest()
        {
            throw new NotImplementedException("Integration test required.");
        }

        #region IsFormulaValidTests

        [TestMethod]
        public void IsFormulaValidTest_ShouldFail_When_Unaccepted_Symbols_Present()
        {
            // Arrange
            var formula = "5+7&";
            // Act
            var result = ProcessingHelper.IsFormulaValid(formula);
            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsFormulaValidTest_ShouldFail_When_Empty_Parentheses_Present()
        {
            // Arrange
            var formula = "y+19+()";
            // Act
            var result = ProcessingHelper.IsFormulaValid(formula);
            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsFormulaValidTest_ShouldFail_When_Parentheses_Disbalanced()
        {
            // Arrange
            var formula = "(5+x/11";
            // Act
            var result = ProcessingHelper.IsFormulaValid(formula);
            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsFormulaValidTest_ShouldFail_When_Multiple_Operators_Stuck_Together()
        {
            // Arrange
            var formula = "429/*11";
            // Act
            var result = ProcessingHelper.IsFormulaValid(formula);
            // Assert
            Assert.IsFalse(result);
        }

        #endregion
    }
}